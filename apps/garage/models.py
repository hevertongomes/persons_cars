from django.db import models
from apps.persons.models import Person

# Create your models here.

class Garage(models.Model):
    name = models.CharField('Nome da garagem', max_length=250)
    capacity = models.IntegerField('Capacidade da garagem')
    person = models.ForeignKey(Person, on_delete=models.CASCADE,null=True, blank=True)
    ativo = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class Vehicle(models.Model):

    name = models.CharField('Nome do carro', max_length=250)
    fabrication_date = models.DateTimeField('Data de fabricação')
    color = models.CharField('Color', null=True, max_length=100)
    brand = models.CharField('Marva', null=True, max_length=150)
    type_vehicle = models.CharField('Tipo de veículo',  null=True, max_length=150)
    garage = models.ForeignKey(Garage, on_delete=models.CASCADE,null=True, blank=True)
    ativo = models.BooleanField(default=True)