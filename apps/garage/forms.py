from django.forms import ModelForm
from .models import Vehicle, Garage

class VehicleForm(ModelForm):
    class Meta:
        model = Vehicle
        fields = ['name', 'fabrication_date', 'color', 'brand', 'type_vehicle', 'garage']

class GarageForm(ModelForm):
    class Meta:
        model = Garage
        fields = ['name', 'capacity']
    