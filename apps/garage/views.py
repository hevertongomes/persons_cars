from django.shortcuts import render, redirect, get_object_or_404
from django.shortcuts import render
from .forms import VehicleForm, GarageForm
from .models import Vehicle, Garage
# Create your views here.


def listvehicles(request):
    vehicles = Vehicle.objects.filter(ativo=True)
    return render(request, 'homevehicle.html', {'vehicles':vehicles})

def createvehicle(request):
    garages = Garage.objects.all()
    if request.method == 'GET':
        return render(request, 'createvehicle.html', {'form':VehicleForm(), 'garages':garages})
    else:
        try:
            print(request.POST)
            form = VehicleForm(request.POST)
            newvehicle = form.save(commit=False)
            newvehicle.save()
            return redirect('listvehicles')
        except ValueError as e:
            return render(request, 'createvehicle.html', {'form':VehicleForm(), 'error': e, 'garages':garages})

def deletevehicle(request, vehicle_id):
    vehicle = get_object_or_404(Vehicle, pk=vehicle_id)
    vehicle.ativo = False
    vehicle.save(update_fields=['ativo'])
    return redirect('listvehicles')


def editvehicle(request, vehicle_id):
    vehicle = get_object_or_404(Vehicle, pk=vehicle_id)
    form = VehicleForm(instance=vehicle)
    garages = Garage.objects.all()
    
    if request.method == 'GET':
        return render(request, 'editvehicle.html', {'vehicle':vehicle, 'form':form, 'garages':garages})
    else:
        form_save = VehicleForm(request.POST, instance=vehicle)
        if form_save.is_valid():
            form_save.save()
        return redirect('listvehicles')


def listgarages(request):
    garages = Garage.objects.filter(ativo=True)
    return render(request, 'homegarage.html', {'garages':garages})

def creategarage(request):
    if request.method == 'GET':
        return render(request, 'creategarage.html', {'form':GarageForm()})
    else:
        try:
            form = GarageForm(request.POST)
            newgarage = form.save(commit=False)
            newgarage.save()
            return redirect('listgarages')
        except ValueError as e:
            return render(request, 'creategarage.html', {'form':GarageForm(), 'error': e})


def deletegarage(request, garage_id):
    garage = get_object_or_404(Garage, pk=garage_id)
    garage.ativo = False
    garage.save(update_fields=['ativo'])
    return redirect('listgarages')


def editgarage(request, garage_id):
    garage = get_object_or_404(Garage, pk=garage_id)
    form = GarageForm(instance=garage)
    
    if request.method == 'GET':
        return render(request, 'editgarage.html', {'garage':garage, 'form':form})
    else:
        form_save = GarageForm(request.POST, instance=garage)
        if form_save.is_valid():
            form_save.save()
        return redirect('listgarages')
