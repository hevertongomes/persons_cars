from rest_framework import routers
from apps.api.views import PersonGarageViewSet

router = routers.SimpleRouter()
router.register('person_cars', PersonGarageViewSet)

urlpatterns = router.urls