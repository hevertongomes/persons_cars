from rest_framework import viewsets, status
from rest_framework.views import APIView
from rest_framework.response import Response
from apps.api.pagination import MediumPagination
from apps.garage.models import Garage
from apps.persons.models import Person
from .serializers import GarageSerializer, PersonSerializer


class PersonGarageViewSet(viewsets.ModelViewSet):
    serializer_class = GarageSerializer
    queryset = Garage.objects.all()
    pagination_class = MediumPagination


    def create(self, request, *args, **kwargs):
            
        person_data = request.data.get('person')
        garage_data = request.data.get('garage')

        person_serializer = PersonSerializer(data=person_data)
        person_serializer.is_valid(raise_exception=True)
        person = person_serializer.save()

        garage_serializer = GarageSerializer(data=garage_data)
        garage_serializer.is_valid(raise_exception=True)
        garage = garage_serializer.save()

        garage.person = person
        garage.save()

        return Response(status=status.HTTP_201_CREATED)
    
    def update(self, request, *args, **kwargs):
            
        person_data = request.data.get('person')
        garage_data = request.data.get('garage')

        if person_data:
            id_person = person_data.pop('id')
            person = Person.objects.get(id=id_person)
            person_serializer = personSerialier(person, data=person_data, partial=partial)
            person_serializer.is_valid(raise_exception=True)
            person_serializer.save()
        if garage_data:
            id_garage = garage_data.pop('id')
            garage = Garage.objects.get(id=id_garage)
            garage_serializer = garageSerialier(garage, data=garage_data, partial=partial)
            garage_serializer.save()
            garage.person = person
            garage.save()

        return Response(status=status.HTTP_200_OK)

