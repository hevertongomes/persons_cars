from django.shortcuts import render, redirect, get_object_or_404
from django.shortcuts import render
from .forms import PersonForm

# Create your views here.

def createperson(request):
    if request.method == 'GET':
        return render(request, 'createperson.html', {'form':PersonForm()})
    else:
        try:
            form = PersonForm(request.POST)
            newperson = form.save(commit=False)
            newperson.save()
            return redirect('/')
        except ValueError:
            return render(request, 'createperson.html', {'form':PersonForm(), 'error': 'Nome longo demais!'})
