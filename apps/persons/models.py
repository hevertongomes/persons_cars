from django.db import models

# Create your models here.
class Person(models.Model):
    name = models.CharField('Nome', max_length=150)
    phone = models.CharField('Telefone', max_length=12)
    email = models.EmailField('Email', max_length=250)
    ativo = models.BooleanField(default=True)
