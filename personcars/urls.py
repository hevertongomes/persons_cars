from django.contrib import admin
from django.urls import path, include
from apps.persons import views as views_person
from apps.core import views as views_core
from apps.garage import views as views_garage
from apps.core import views as views_core

urlpatterns = [
    path('admin/', admin.site.urls),

    #Auth

    #Person
    path('', views_core.home, name='home'),
    path('createperson/', views_person.createperson, name='createperson'),

    #Vehicle
    path('listvehicle/', views_garage.listvehicles, name='listvehicles'),
    path('createvehicle/', views_garage.createvehicle, name='createvehicle'),
    path('deletevehicle/<int:vehicle_id>', views_garage.deletevehicle, name='deletevehicle'),
    path('editvehicle/<int:vehicle_id>', views_garage.editvehicle, name='editvehicle'),

    #Garage
    path('listgarage/', views_garage.listgarages, name='listgarages'),
    path('creategarage/', views_garage.creategarage, name='creategarage'),
    path('deletegarage/<int:garage_id>', views_garage.deletegarage, name='deletegarage'),
    path('editgarage/<int:garage_id>', views_garage.editgarage, name='editgarage'),

    # API
    path('person_cars/', include('apps.api.urls')),

]
